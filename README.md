# hello-graal

The "classical" Java hello-world, compiled to native using [GraalVM](https://www.graalvm.org/).

Although this might sound simple, there are a lot of things going on here and if you like this project, please drop a star.

## Features:
* A nice CI pipeline, running on GitLab.
* Builds run using a [custom Docker image](https://hub.docker.com/r/raonigabriel/graalvm-playground) that I developed which includes JDK, Maven, GraalVM and some extras. 
* Maven [resource filtering](https://maven.apache.org/plugins/maven-resources-plugin/examples/filter.html) which I think is a nice feature usually overlooked by developers.
* Maven shade plugin, to repackage everything as an ubber-jar but also to make it runnable (MANIFEST.MF) and to also remove some useless Maven metadata.
* A custom Maven profile which is automatically triggered when it detects the $GRAALVM_HOME environment variable, otherwise the required tools wouldn't be available.
* We use native-image tool (from GraalVM) to generate a statically-linked native binary, hence no external system libraries will be required.
* We also run strip to discard symbols from the generated binary.
* Then we run [upx](https://upx.github.io/) to re-package this binary, further reducing its size.
* Finally, we generate a Debian package (*.deb) that can be used to install this tool. 

## Installation on Debian distros:
Download and install [hello-graal_1.0.0_amd64.deb](https://gitlab.com/raonigabriel/hello-graal/-/jobs/artifacts/master/raw/target/hello-graal_1.0.0_amd64.deb?job=build-binary)

```
$ curl -L https://gitlab.com/raonigabriel/hello-graal/-/jobs/artifacts/master/raw/target/hello-graal_1.0.0_amd64.deb?job=build-binary --output hello-graal_1.0.0_amd64.deb
...
$ sudo dpkg -i hello-graal_1.0.0_amd64.deb
...
$ hello-graal
Hello, Graal!
$ sudo dpkg --remove hello-graal
```
---

## License

Released under the [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0.html)
